import { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import ResultList from './components/ResultList';
import SearchForm from './components/SearchForm';
import { searchMovies, searchSimilarMovie } from './rest/movies';

function App() {
  const [movies, setMovies] = useState();
  const [searchParams, setSearchParams] = useState({ title: '', page: 1 });
  const [disableSearch, setDisableSearch] = useState(false);

  const handlePageChange = (_, newPage) => {
    setSearchParams(prevState => ({ title: prevState.title, page: newPage }));
  };

  const handleSearch = searchTitle => {
    setSearchParams({ title: searchTitle, page: 1 });
  };

  useEffect(() => {
    searchMovies(searchParams).then(data => {
      setMovies(data);
    });
  }, [searchParams]);

  const handleSearchSimilar = (_, id) => {
    searchSimilarMovie(id).then(data => {
      setDisableSearch(true);
      setMovies(data);
    });
  };

  const handleBackToSearch = () => {
    console.log('asd');
    setDisableSearch(false);
    setSearchParams({ title: '', page: 1 });
    setMovies(null);
  };

  return (
    <Box className="app">
      <SearchForm
        searcMovies={handleSearch}
        disableSearch={disableSearch}
        onClickBackToSearch={handleBackToSearch}
      />
      {movies && (
        <ResultList
          movies={movies.results}
          pages={movies.total_pages}
          onPageChange={handlePageChange}
          onSearchSimilar={handleSearchSimilar}
          actualPage={searchParams.page}
        />
      )}
    </Box>
  );
}

export default App;
