import axios from 'axios';

const searchMovies = ({ title, page }) => {
  const params = {
    query: title,
    page
  };
  return axios.get('/movies/search', { params }).then(response => response.data);
};

const searchMovie = id => {
  return axios.get(`/movies/search/${id}`).then(response => response.data);
};

const searchSimilarMovie = id => {
  return axios.get(`/movies/similar/${id}`).then(response => response.data);
};

export { searchMovies, searchMovie, searchSimilarMovie };
