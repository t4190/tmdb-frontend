import { useState } from 'react';
import { Paper, Grid, Typography, Divider, Dialog, Pagination } from '@mui/material';
import MovieItem from './MovieItem';
import MovieDetails from './MovieDetails';

function ResultList({ movies, pages, onPageChange, actualPage, onSearchSimilar }) {
  const [movieId, setMovieId] = useState(null);

  const handleChangeMovieId = id => {
    setMovieId(id);
  };

  const handleClose = () => {
    setMovieId(null);
  };

  return (
    <Paper elevation={2} className="result-list">
      <Grid container>
        <Grid item xs={4}>
          <Typography variant="h5" component="h5">
            Movie title
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="h5" component="h5">
            Rating
          </Typography>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="h5" component="h5">
            Release date
          </Typography>
        </Grid>
      </Grid>
      <Divider />
      {movies.map((movie, index) => (
        <MovieItem key={index} movie={movie} onChangeMoviId={handleChangeMovieId} />
      ))}
      <Pagination count={pages} color="primary" onChange={onPageChange} page={actualPage} />
      <Dialog
        open={!!movieId}
        onClose={handleClose}
        aria-labelledby="dialog"
        aria-describedby="dialog"
      >
        <MovieDetails id={movieId} onClose={handleClose} onSearchSimilar={onSearchSimilar} />
      </Dialog>
    </Paper>
  );
}

export default ResultList;
