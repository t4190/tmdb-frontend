import { useState } from 'react';
import { Paper, Typography, TextField, Button } from '@mui/material';

function SearchForm({ searcMovies, disableSearch, onClickBackToSearch }) {
  const [title, setTitle] = useState('');

  const handleChangeTitle = event => {
    setTitle(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();
    searcMovies(title);
  };

  const handleBackToSearch = () => {
    console.log('asd');
    setTitle('');
    onClickBackToSearch();
  };

  return (
    <Paper elevation={2} className="search-form" component="form" onSubmit={handleSubmit}>
      {disableSearch ? (
        <Button variant="contained" className="search-submit" onClick={handleBackToSearch}>
          Back to search
        </Button>
      ) : (
        <>
          <Typography variant="h4" component="h4" className="title">
            Search for movies
          </Typography>
          <TextField
            className="search-field"
            value={title}
            onChange={handleChangeTitle}
            variant="outlined"
          />
          <Button type="submit" variant="contained" className="search-submit" disabled={!title}>
            Search
          </Button>
        </>
      )}
    </Paper>
  );
}

export default SearchForm;
