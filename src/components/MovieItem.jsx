import { Grid, Divider } from '@mui/material';

function MovieItem({ movie: { original_title, vote_average, release_date, id }, onChangeMoviId }) {
  return (
    <>
      <Grid container>
        <Grid item xs={4} rowSpacing={2} columnSpacing={2}>
          <span className="movie-title" onClick={() => onChangeMoviId(id)}>
            {original_title}
          </span>
        </Grid>
        <Grid item xs={4}>
          {vote_average}
        </Grid>
        <Grid item xs={4}>
          {release_date}
        </Grid>
      </Grid>
      <Divider />
    </>
  );
}

export default MovieItem;
