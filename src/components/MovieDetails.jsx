import { useEffect, useState } from 'react';
import { DialogTitle, Typography, DialogContent, DialogActions, Button } from '@mui/material';
import { searchMovie } from '../rest/movies';

function MovieDetails({ id, onClose, onSearchSimilar }) {
  const [movie, setMovie] = useState();

  useEffect(() => {
    searchMovie(id).then(data => setMovie(data));
  }, [id]);

  const hadnleSimilarSearch = event => {
    onClose();
    onSearchSimilar(event, id);
  };
  return movie ? (
    <>
      <DialogTitle id="dialog-title">
        Title: {movie.title}({movie.original_title})
      </DialogTitle>
      <DialogContent dividers>
        <div elevation={2} className="movie-details">
          <div className="detail">
            <Typography variant="span">{movie.overview}</Typography>
          </div>
          <div className="detail">
            <Typography variant="span">{movie.release_date}</Typography>
          </div>
          <div className="detail">
            <Typography variant="span">Status: {movie.status}</Typography>
          </div>
          <div className="detail">
            <Typography variant="span">Genre: </Typography>
            {movie.genres.map(genre => (
              <Typography variant="span" key={genre.id}>
                {genre.name}{' '}
              </Typography>
            ))}
          </div>
          <div className="detail">
            <Typography variant="span">Spoken languages: </Typography>
            {movie.spoken_languages.map((lang, index) => (
              <Typography variant="span" key={index}>
                {lang.english_name}{' '}
              </Typography>
            ))}
          </div>
          <div className="detail">
            <a
              href={`https://www.imdb.com/title/${movie.imdb_id}`}
              target="_blank"
              rel="noreferrer"
            >
              IMDB link
            </a>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} variant="contained">
          Close
        </Button>
        <Button onClick={hadnleSimilarSearch} variant="contained">
          Similar movies
        </Button>
      </DialogActions>
    </>
  ) : null;
}

export default MovieDetails;
